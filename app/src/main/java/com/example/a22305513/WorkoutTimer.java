package com.example.a22305513;

import android.os.CountDownTimer;
import android.widget.TextView;

public class WorkoutTimer extends CountDownTimer {
    long timeLeft; //time left in timer (used for resuming)
    boolean isPaused;
    boolean hasStarted;
    boolean hasFinished; //used for reloading UI
    WorkoutTimer resumedTimer; //new timer using timeLeft (for resuming)
    TextView timerDisplay; //text view from main activity to update
    MainActivity mainActivity; //needed for calling updateUI

    public WorkoutTimer(long millisInFuture, long countDownInterval, TextView timerDisplay, MainActivity mainActivity) {
        super(millisInFuture, 1000);
        this.timeLeft = millisInFuture;
        this.isPaused = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.timerDisplay = timerDisplay;
        this.mainActivity = mainActivity;
    }

    @Override
    public void onTick(long timeLeft) {
        this.timeLeft = timeLeft;
        long min = timeLeft / 60000;
        String minDisplay = String.valueOf(min);
        if (min < 10) {
            minDisplay = "0" + minDisplay;
        }

        long sec = (timeLeft % 60000) / 1000;
        String secDisplay = String.valueOf(sec);
        if (sec < 10) {
            secDisplay = "0" + secDisplay;
        }

        timerDisplay.setText(minDisplay + ":" + secDisplay);
    }

    @Override
    public void onFinish() {
        skipTimer();
        MainActivity.playingTimer = null;
        mainActivity.updateUI(MainActivity.counter); //needed so timer continues to next when done
    }

    public void playTimer() {
        hasFinished = false;
        if (!hasStarted) {
            start();
            hasStarted = true;
        } else {
            //if timer has already started, create new timer
            resumedTimer = new WorkoutTimer(
                    timeLeft, 1000, timerDisplay, mainActivity);
            isPaused = false;
        }
    }
    public void pauseTimer() {
        cancel();
        isPaused = true;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public boolean hasStarted() {
        return hasStarted;
    }

    public void skipTimer() {
        cancel();
        resetTimer();
        MainActivity.counter ++;
    }

    //reset timer variables to default
    public void resetTimer() {
        isPaused = false;
        hasStarted = false;
    }

}
