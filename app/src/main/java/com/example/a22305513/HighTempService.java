package com.example.a22305513;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.widget.Toast;

public class HighTempService extends Service {

    SensorManager sensorMan;
    private boolean isTooHot = false;
    private static final float TOO_HOT = 40; //40C

    @Override
    public void onCreate() {
        super.onCreate();
        sensorMan = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
    }

    final SensorEventListener tempSensorListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // do nothing
        }

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            float temperature = sensorEvent.values[0];

            if (temperature > TOO_HOT) {
                if (!isTooHot) {
                    isTooHot = true;
                    Toast.makeText(getBaseContext(), R.string.tempWarning, Toast.LENGTH_LONG).show();
                    if (MainActivity.playingTimer != null) {
                        MainActivity.playingTimer.pauseTimer();
                    }
                }
            } else {
                isTooHot = false;
            }
        }

    };

    @Override
    public void onDestroy() {
        sensorMan.unregisterListener(tempSensorListener);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Sensor tempSensor = sensorMan.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sensorMan.registerListener(tempSensorListener, tempSensor, SensorManager.SENSOR_DELAY_FASTEST);
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }
}
