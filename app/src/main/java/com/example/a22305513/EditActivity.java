package com.example.a22305513;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class EditActivity extends AppCompatActivity {

    EditText newNameText; //field for inputting new timer name
    NumberPicker minutePicker;
    NumberPicker secondPicker;
    ImageButton addTimerButton;
    ImageButton workModeButton; //button for switching to workout mode
    LinearLayout timerList; //layout to add new playlist entries to

    TimersDB timerDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        newNameText = (EditText) findViewById(R.id.newName);
        minutePicker = (NumberPicker) findViewById(R.id.minutes);
        secondPicker = (NumberPicker) findViewById(R.id.seconds);
        addTimerButton = (ImageButton) findViewById(R.id.addTimer);
        workModeButton = (ImageButton) findViewById(R.id.workout);
        timerList = (LinearLayout) findViewById(R.id.timerList);

        //number picker settings
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(15);
        secondPicker.setMinValue(0);
        secondPicker.setMaxValue(59);

        timerDB = new TimersDB(getApplicationContext());

        workModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent workActivityIntent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(workActivityIntent);
            }
        });

        addTimerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNameSet() && isLengthSet()) {
                    save();
                    addToLayout(timerDB.getCurrentID());
                    newNameText.setText("");
                } else {
                    Toast.makeText(getBaseContext(), R.string.noTimerData, Toast.LENGTH_LONG).show();
                }
                minutePicker.setValue(0);
                secondPicker.setValue(0);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        String[] savedTimers = timerDB.getAllNames();
        int length = savedTimers.length;

        for (int i = 0; i < length; i++) {
            reloadToLayout(timerDB.getID(savedTimers[i]));
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int minutes = savedInstanceState.getInt("minute_picker");
        int seconds = savedInstanceState.getInt("second_picker");

        minutePicker.setValue(minutes);
        secondPicker.setValue(seconds);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("minute_picker", minutePicker.getValue());
        outState.putInt("second_picker", secondPicker.getValue());
        super.onSaveInstanceState(outState);
    }

    private boolean isNameSet() {
        if (newNameText.getText().toString().contentEquals("")) {
            return false;
        }
        return true;
    }

    private boolean isLengthSet() {
        if (minutePicker.getValue() == 0 && secondPicker.getValue() == 0) {
            return false;
        }
        return true;
    }

    private void save() {
        String timerName = newNameText.getText().toString();
        int minutes = minutePicker.getValue();
        int seconds = secondPicker.getValue();
        int timerLength = (minutes * 60) + seconds;

        timerDB.addRow(timerName, timerLength);
    }

    private void addToLayout(int idNum) {
        String timerName = newNameText.getText().toString();
        String minString = "00";
        String secString = "00";

        int minutes = minutePicker.getValue();
        if (minutes < 10) {
            minString = "0" + minutes;
        } else {
            minString = String.valueOf(minutes);
        }

        int seconds = secondPicker.getValue();
        if (seconds < 10) {
            secString = "0" + seconds;
        } else {
            secString = String.valueOf(seconds);
        }

        //calculate dp
        int screenPixelDensity = (int) getResources().getDisplayMetrics().density;
        int standard = 60 * screenPixelDensity;

        //relative layout to hold timer info and delete button
        final RelativeLayout listItem = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, standard
        );
        layoutParams.setMargins(0, 15, 0, 15 );
        listItem.setLayoutParams(layoutParams);
        listItem.setBackgroundColor(getColor(R.color.white));

        timerList.addView(listItem);

        //text view to display timer name
        TextView timerNameView = new TextView(this);
        timerNameView.setText(timerName);
        timerNameView.setTextAppearance(R.style.ListFont);
        //layout params
        RelativeLayout.LayoutParams nameParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        nameParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        nameParams.addRule(RelativeLayout.CENTER_VERTICAL);
        timerNameView.setLayoutParams(nameParams);

        listItem.addView(timerNameView);

        //text view to display timer length
        TextView timerLengthView = new TextView(this);
        timerLengthView.setText(minString + ":" + secString);
        timerLengthView.setTextAppearance(R.style.ListFont);
        //layout params
        RelativeLayout.LayoutParams lengthParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        lengthParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        timerLengthView.setLayoutParams(lengthParams);

        listItem.addView(timerLengthView);

        //button to delete timer
        ImageButton deleteButton = new ImageButton(this);
        deleteButton.setBackground(getDrawable(R.drawable.delete));
        //layout params
        RelativeLayout.LayoutParams deleteParams = new RelativeLayout.LayoutParams(
                standard, standard
        );
        deleteParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        deleteParams.addRule(RelativeLayout.CENTER_VERTICAL);
        deleteButton.setLayoutParams(deleteParams);

        listItem.addView(deleteButton);

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int specificID = timerDB.getID(String.valueOf(timerNameView.getText()));
                timerDB.deleteRow(specificID);
                timerList.removeView(listItem);
            }
        });
    }

    private void reloadToLayout(int idNum) {
        String timerName = timerDB.getName(idNum);
        String minString = "00";
        String secString = "00";

        int minutes = timerDB.getLength(idNum) / 60;
        if (minutes < 10) {
            minString = "0" + minutes;
        } else {
            minString = String.valueOf(minutes);
        }

        int seconds = timerDB.getLength(idNum) % 60;
        if (seconds < 10) {
            secString = "0" + seconds;
        } else {
            secString = String.valueOf(seconds);
        }

        //calculate dp
        int screenPixelDensity = (int) getResources().getDisplayMetrics().density;
        int standard = 60 * screenPixelDensity;

        //relative layout to hold timer info and delete button
        final RelativeLayout listItem = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, standard
        );
        layoutParams.setMargins(0, 15, 0, 15 );
        listItem.setLayoutParams(layoutParams);
        listItem.setBackgroundColor(getColor(R.color.white));

        timerList.addView(listItem);

        //text view to display timer name
        TextView timerNameView = new TextView(this);
        timerNameView.setText(timerName);
        timerNameView.setTextAppearance(R.style.ListFont);
        //layout params
        RelativeLayout.LayoutParams nameParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        nameParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        nameParams.addRule(RelativeLayout.CENTER_VERTICAL);
        timerNameView.setLayoutParams(nameParams);

        listItem.addView(timerNameView);

        //text view to display timer length
        TextView timerLengthView = new TextView(this);
        timerLengthView.setText(minString + ":" + secString);
        timerLengthView.setTextAppearance(R.style.ListFont);
        //layout params
        RelativeLayout.LayoutParams lengthParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        lengthParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        timerLengthView.setLayoutParams(lengthParams);

        listItem.addView(timerLengthView);

        //button to delete timer
        ImageButton deleteButton = new ImageButton(this);
        deleteButton.setBackground(getDrawable(R.drawable.delete));
        //layout params
        RelativeLayout.LayoutParams deleteParams = new RelativeLayout.LayoutParams(
                standard, standard
        );
        deleteParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        deleteParams.addRule(RelativeLayout.CENTER_VERTICAL);
        deleteButton.setLayoutParams(deleteParams);

        listItem.addView(deleteButton);

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int specificID = timerDB.getID(String.valueOf(timerNameView.getText()));
                timerDB.deleteRow(specificID);
                timerList.removeView(listItem);
            }
        });
    }

}
